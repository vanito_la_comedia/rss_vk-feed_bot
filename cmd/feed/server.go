package main

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/feed"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/database"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc"
	"log"
	"net"
	"net/http"
	"os"
)

func main() {

	b, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.ParseConfig(b)
	if err != nil {
		log.Fatal(err)
	}

	dsn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		cfg.Database.Host, cfg.Database.Port, cfg.Database.User, cfg.Database.Password, cfg.Database.Dbname, cfg.Database.Sslmode)
	log.Println("dsn=", dsn)

	ctx := context.Background()

	db, err := database.New(ctx, dsn)
	if err != nil {
		log.Fatalf("failed init postgres :%v", err)
	}
	defer db.Close()

	repo := repository.New(db)
	newServer := feed.New(repo)

	feedURL := fmt.Sprintf("%s:%d", cfg.Feed.Host, cfg.Feed.Port)
	gatewayURL := fmt.Sprintf("%s:%d", cfg.Gateway.Host, cfg.Gateway.Port)
	log.Println("start on ", feedURL)

	gatewayServer := feed.NewGatewayServer(ctx, feedURL)

	go func() {
		log.Println("Gateway server start on ", gatewayURL)
		if err := http.ListenAndServe(gatewayURL, gatewayServer); err != nil {
			log.Fatal("Failed start gateway server")
		}
	}()

	lis, err := net.Listen("tcp", feedURL)

	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	telToken := os.Getenv("TELEGRAM_APITOKEN")

	if telToken == "" {
		log.Fatalf("empty telegram token")
	}

	vkToken := os.Getenv("VK_ACCESS_TOKEN")

	if vkToken == "" {
		log.Fatalf("empty vk token")
	}

	cfg.VkToken = vkToken
	cfg.TelToken = telToken

	if cfg.Feed.WCount < 1 {
		log.Fatalf("invalid workers count")
	}
	log.Println("workers count =", cfg.Feed.WCount)

	go feed.Check(ctx, repo, cfg)

	grpcServer := grpc.NewServer()
	pb.RegisterFeedServer(grpcServer, newServer)
	err = grpcServer.Serve(lis)
	if err != nil {
		log.Fatalf("failed to serve grpc server: %v", err)
	}

}
