package main

import (
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/telegram"
	"log"
	"os"
)

// util for test telegram api. Skip it on review
func main() {
	token := os.Getenv("TELEGRAM_APITOKEN")

	if token == "" {
		log.Fatalf("empty bot token")
	}

	if err := telegram.SendOneNews(513805209, "new news1", token); err != nil {
		log.Panicln(err)
	}
}
