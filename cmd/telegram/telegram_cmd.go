package main

import (
	"context"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
	"os"
	"strconv"
	"strings"
)

const hello = "Hello!👋\nThis is rss vk feed bot. You can subscribe for rss url source or open vk group source. \n" +
	"\nPrint /help for more information."

const help = "This is bot for checking news from rss and vk groups\n List of available commands:\n" +
	"/help - show help with list of available commands \n" +
	"/list - show list of current subscriptions \n" +
	"/addsource <type> <url> - add subscription for url. <type> may be 'rss' or 'vk'\n" +
	"/delsource <id> - delete subscription for <id>. Use /list command to check current subscriptions first \n" +
	"/addkeyword <id> <keyword> - add search keyword for subscription. Bot will send news from subscription which include keyword \n" +
	"/delkeyword <id> - delete search keyword for subscription.\n"

func main() {

	b, err := os.ReadFile("./config/config.yaml")
	if err != nil {
		log.Fatal(err)
	}

	cfg, err := config.ParseConfig(b)
	if err != nil {
		log.Fatal(err)
	}

	cfg.TelToken = os.Getenv("TELEGRAM_APITOKEN")
	if cfg.TelToken == "" {
		log.Fatal("empty bot token")
	}

	if err := runBot(cfg); err != nil {
		log.Fatal(err)
	}
}

func getGrpcCliConn(cfg *config.Config) (*grpc.ClientConn, error) {
	tcURL := fmt.Sprintf("%s:%d", cfg.Telegram.Host, cfg.Telegram.Port)
	log.Println("start on ", tcURL)

	conn, err := grpc.Dial(tcURL, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, fmt.Errorf("can't dial to grpc")
	}
	return conn, nil
}

func listCommand(ctx context.Context, client api.FeedClient, chatID int64) string {
	resp, err := client.ListSource(ctx, &api.ListReq{ChatID: chatID})

	if err != nil || resp.Sources == nil {
		return "List is empty"
	}

	var sb strings.Builder
	for _, r := range resp.Sources {
		sb.WriteString(fmt.Sprintf("ID=%d Type=%s URL=%s Keyword=%s \n", r.ID, r.Type, r.URL, r.Keyword))
	}
	return sb.String()
}

func addSourceCommand(ctx context.Context, client api.FeedClient, chatID int64, args string) string {

	if args == "" {
		return "Invalid count of arguments"
	}

	slArgs := strings.Split(args, " ")
	if len(slArgs) != 2 {
		return "Invalid count of arguments"
	}

	if slArgs[0] != "vk" && slArgs[0] != "rss" {
		return "Invalid type. You should use 'vk' or 'rss'"
	}

	resp, err := client.AddSource(ctx, &api.AddSoRequest{Type: slArgs[0], URL: slArgs[1], ChatID: chatID})
	if err != nil {
		return "Can't add source"
	}

	return fmt.Sprintf("Added new source with ID %d", resp.ID)
}

func delSourceCommand(ctx context.Context, client api.FeedClient, chatID int64, args string) string {

	if args == "" {
		return "Invalid count of arguments"
	}

	slArgs := strings.Split(args, " ")
	if len(slArgs) != 1 {
		return "Invalid count of arguments"
	}

	id, _ := strconv.ParseInt(slArgs[0], 10, 64)
	_, err := client.DelSource(ctx, &api.ID{ID: id, ChatID: chatID})

	if err != nil {
		return fmt.Sprintf("Source with ID %s doesn't exist", slArgs[0])
	}

	return fmt.Sprintf("Delete source with ID %s", slArgs[0])
}

func addKeywordCommand(ctx context.Context, client api.FeedClient, chatID int64, args string) string {

	if args == "" {
		return "Invalid count of arguments"
	}

	slArgs := strings.Split(args, " ")
	if len(slArgs) != 2 {
		return "Invalid count of arguments"
	}

	id, _ := strconv.ParseInt(slArgs[0], 10, 64)
	resp, err := client.AddKeyword(ctx, &api.AddKeyRequest{ID: id, Keyword: slArgs[1], ChatID: chatID})

	if err != nil {
		return "Can't add keyword"
	}

	return fmt.Sprintf("Added new keyword for ID %d", resp.ID)
}

func delKeywordCommand(ctx context.Context, client api.FeedClient, chatID int64, args string) string {

	if args == "" {
		return "Invalid count of arguments"
	}

	slArgs := strings.Split(args, " ")
	if len(slArgs) != 1 {
		return "Invalid count of arguments"
	}

	id, _ := strconv.ParseInt(slArgs[0], 10, 64)
	_, err := client.DelKeyword(ctx, &api.ID{ID: id, ChatID: chatID})

	if err != nil {
		return fmt.Sprintf("Source with ID %s doesn't exist", slArgs[0])
	}

	return fmt.Sprintf("Delete keyword for ID %s", slArgs[0])
}

func runBot(cfg *config.Config) error {

	conn, err := getGrpcCliConn(cfg)
	if err != nil {
		return err
	}
	defer conn.Close()
	client := api.NewFeedClient(conn)

	ctx := context.Background()
	bot, err := tgbotapi.NewBotAPI(cfg.TelToken)
	if err != nil {
		return err
	}
	bot.Debug = true
	log.Printf("Authorized on account %s", bot.Self.UserName)

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60
	updates := bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil {
			continue
		}

		msg := tgbotapi.NewMessage(update.Message.Chat.ID, update.Message.Text)

		switch update.Message.Command() {
		case "start":
			msg.Text = hello
		case "help":
			msg.Text = help
		case "list":
			msg.Text = listCommand(ctx, client, update.Message.Chat.ID)
		case "addsource":
			msg.Text = addSourceCommand(ctx, client, update.Message.Chat.ID, update.Message.CommandArguments())
		case "delsource":
			msg.Text = delSourceCommand(ctx, client, update.Message.Chat.ID, update.Message.CommandArguments())
		case "addkeyword":
			msg.Text = addKeywordCommand(ctx, client, update.Message.Chat.ID, update.Message.CommandArguments())
		case "delkeyword":
			msg.Text = delKeywordCommand(ctx, client, update.Message.Chat.ID, update.Message.CommandArguments())
		default:
			msg.Text = "Unknown command. Please check /help"
		}
		if _, err := bot.Send(msg); err != nil {
			log.Panic(err)
		}
	}
	return nil
}
