package main

import (
	"context"
	"database/sql"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/vk"
	"log"
	"os"
	"time"
)

// util for test vk api. Skip it on review

func main() {
	ctx := context.Background()

	vkToken := os.Getenv("VK_ACCESS_TOKEN")

	if vkToken == "" {
		log.Fatalf("empty vk token")
	}

	vk.GetNews(ctx, "https://vk.com/tr_ryazan", sql.NullTime{Time: time.Now()}, vkToken)

}
