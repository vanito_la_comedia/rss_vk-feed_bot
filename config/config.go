package config

import (
	"gopkg.in/yaml.v3"
)

type Feed struct {
	UpdateTimeout int    `yaml:"updateTimeout"`
	Host          string `yaml:"host"`
	Port          int    `yaml:"port"`
	WCount        int    `yaml:"wCount"`
}

type Telegram struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type Database struct {
	Host     string `yaml:"host"`
	Port     int    `yaml:"port"`
	User     string `yaml:"user"`
	Password string `yaml:"password"`
	Dbname   string `yaml:"dbname"`
	Sslmode  string `yaml:"sslmode"`
}

type Gateway struct {
	Host string `yaml:"host"`
	Port int    `yaml:"port"`
}

type Config struct {
	Feed     Feed
	Telegram Telegram
	Database Database
	Gateway  Gateway
	VkToken  string
	TelToken string
}

func ParseConfig(fileBytes []byte) (*Config, error) {
	cf := Config{}

	err := yaml.Unmarshal(fileBytes, &cf)
	if err != nil {
		return nil, err
	}
	return &cf, nil
}
