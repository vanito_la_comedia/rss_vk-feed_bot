protoc -I. --go_out=pkg --go_opt=paths=source_relative --go-grpc_out=pkg --go-grpc_opt=paths=source_relative \
--grpc-gateway_out=pkg --grpc-gateway_opt logtostderr=true --grpc-gateway_opt paths=source_relative \
--swagger_out=pkg/api --swagger_opt allow_merge=true --swagger_opt merge_file_name=api api/api.proto