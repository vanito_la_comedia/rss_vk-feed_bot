CREATE TABLE IF NOT EXISTS sources (
    id          serial          PRIMARY KEY,
    type        varchar(32)     NOT NULL,
    URL         varchar(200)    NOT NULL,
    keyword     varchar(32),
    updated     timestamp       DEFAULT now() NOT NULL,
    chatID      bigint          NOT NULL
);