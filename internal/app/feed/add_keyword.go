package feed

import (
	"context"
	"errors"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (f *feed) AddKeyword(ctx context.Context, req *pb.AddKeyRequest) (*pb.ID, error) {

	var source = models.Source{
		ID:      req.ID,
		ChatID:  req.ChatID,
		Keyword: req.Keyword,
	}

	ID, err := f.feedRepo.AddKeyword(ctx, source)

	if errors.Is(err, repository.ErrNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	return &pb.ID{
		ID:     ID,
		ChatID: req.ChatID,
	}, nil

}
