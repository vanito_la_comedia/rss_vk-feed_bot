package feed

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/mock"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestFeed_AddKeyword(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.AddKeywordMock.Return(5, nil)
	svc := New(mockRepo)

	ctx := context.Background()
	keyReq := api.AddKeyRequest{ChatID: 100, ID: 5, Keyword: "key1"}
	resp, _ := svc.AddKeyword(ctx, &keyReq)

	assert.Equal(t, keyReq.ID, resp.ID, "invalid ID")
}

func TestFeed_AddKeywordNotFound(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.AddKeywordMock.Return(0, repository.ErrNotFound)
	svc := New(mockRepo)

	ctx := context.Background()
	keyReq := api.AddKeyRequest{ChatID: 100, ID: 5, Keyword: "key1"}
	_, err := svc.AddKeyword(ctx, &keyReq)

	assert.Equal(t, err, status.Error(codes.NotFound, repository.ErrNotFound.Error()))
}

func TestFeed_AddKeywordError(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.AddKeywordMock.Return(0, status.Error(codes.Internal, "can't add keyword"))
	svc := New(mockRepo)

	ctx := context.Background()
	keyReq := api.AddKeyRequest{}
	_, err := svc.AddKeyword(ctx, &keyReq)

	assert.Error(t, err)
}
