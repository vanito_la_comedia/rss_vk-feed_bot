package feed

import (
	"context"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (f *feed) AddSource(ctx context.Context, req *pb.AddSoRequest) (*pb.ID, error) {

	var source = models.Source{
		ChatID: req.ChatID,
		Type:   req.Type,
		URL:    req.URL,
	}

	ID, err := f.feedRepo.AddSource(ctx, source)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &pb.ID{
		ID:     ID,
		ChatID: req.ChatID,
	}, nil

}
