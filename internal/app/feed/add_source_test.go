package feed

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/mock"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestFeed_AddSource(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.AddSourceMock.Return(1, nil)
	svc := New(mockRepo)

	ctx := context.Background()
	soReq := api.AddSoRequest{ChatID: 100, Type: "vk", URL: "ololo"}
	resp, _ := svc.AddSource(ctx, &soReq)

	assert.GreaterOrEqualf(t, resp.ID, int64(1), "invalid ID %d", resp.ID)
	assert.Equal(t, soReq.ChatID, resp.ChatID, "invalid ChatID")

}

func TestFeed_AddSourceError(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.AddSourceMock.Return(0, status.Error(codes.Internal, "can't add source"))
	svc := New(mockRepo)

	ctx := context.Background()
	soReq := api.AddSoRequest{}
	_, err := svc.AddSource(ctx, &soReq)

	assert.Error(t, err)
}
