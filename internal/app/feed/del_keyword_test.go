package feed

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/mock"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestFeed_DelKeyword(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.DelKeywordMock.Return(nil)
	svc := New(mockRepo)

	ctx := context.Background()
	keyReq := api.ID{ChatID: 100, ID: 1}
	_, err := svc.DelKeyword(ctx, &keyReq)

	assert.NoError(t, err)

}

func TestFeed_DelKeywordNotFound(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.DelKeywordMock.Return(repository.ErrNotFound)
	svc := New(mockRepo)

	ctx := context.Background()
	keyReq := api.ID{ChatID: 100, ID: 5}
	_, err := svc.DelKeyword(ctx, &keyReq)

	assert.Equal(t, err, status.Error(codes.NotFound, repository.ErrNotFound.Error()))
}
