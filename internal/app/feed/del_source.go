package feed

import (
	"context"
	"errors"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/emptypb"
)

func (f *feed) DelSource(ctx context.Context, req *pb.ID) (*emptypb.Empty, error) {

	var source = models.Source{
		ID:     req.ID,
		ChatID: req.ChatID,
	}

	err := f.feedRepo.DelSource(ctx, source)

	if errors.Is(err, repository.ErrNotFound) {
		return nil, status.Error(codes.NotFound, err.Error())
	}

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &emptypb.Empty{}, nil

}
