package feed

import (
	"context"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/mock"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"testing"
)

func TestFeed_DelSource(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.DelSourceMock.Return(nil)
	svc := New(mockRepo)

	ctx := context.Background()
	soReq := api.ID{ChatID: 100, ID: 1}
	_, err := svc.DelSource(ctx, &soReq)

	assert.NoError(t, err)

}

func TestFeed_DelSourceNotFound(t *testing.T) {

	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	mockRepo.DelSourceMock.Return(repository.ErrNotFound)
	svc := New(mockRepo)

	ctx := context.Background()
	soReq := api.ID{ChatID: 100, ID: 5}
	_, err := svc.DelSource(ctx, &soReq)

	assert.Equal(t, err, status.Error(codes.NotFound, repository.ErrNotFound.Error()))
}
