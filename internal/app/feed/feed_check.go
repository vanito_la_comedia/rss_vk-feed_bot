package feed

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/config"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/rss"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/telegram"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/app/vk"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	"log"
	"strings"
	"sync"
	"time"
)

func Check(ctx context.Context, repo repo.Repository, cfg *config.Config) error {
	for {
		log.Println("FeedCheck")
		chatIDs, err := repo.ListChatID(ctx)
		if err != nil {
			return fmt.Errorf("failed get users: %v", err)
		}
		log.Println("ChatIDs=", chatIDs)
		for _, c := range chatIDs {

			err := CheckChatIDUpdates(ctx, c, repo, cfg)
			if err != nil {
				log.Println("Can't update news for ChatID", c, err)
			}
		}
		time.Sleep(time.Duration(cfg.Feed.UpdateTimeout) * time.Minute)
	}
}

func updateNews(ctx context.Context, source models.Source, repo repo.Repository, cfg *config.Config) {
	log.Println("updateNews source", source.URL)
	var tryNews []string
	var news []string
	switch source.Type {
	case "rss":
		rssNews, err := rss.GetNews(ctx, source.URL, source.Updated)
		if err != nil {
			log.Println("Can't get rss news", err)
		} else {
			tryNews = append(news, rssNews...)
		}
	case "vk":
		vkNews, err := vk.GetNews(ctx, source.URL, source.Updated, cfg.VkToken)
		if err != nil {
			log.Println("Can't get vk news", err)
		} else {
			tryNews = append(news, vkNews...)
		}
	}
	news = SkipNewsByKeyword(tryNews, source.Keyword)
	log.Println("News:= ", news, "from source ", source.URL)
	if len(news) > 0 {
		err := telegram.SendSevNews(source.ChatID, news, cfg.TelToken)
		if err != nil {
			log.Println("Can't send new to telegram chatID", source.ChatID, err)
		}
		err = repo.UpdateTime(ctx, source.ID)
		if err != nil {
			log.Println("Can't update time for ID", source.ID, err)
		}
	}
}

func CheckChatIDUpdates(ctx context.Context, chatID int64, repo repo.Repository, cfg *config.Config) error {
	slS, err := repo.ListSource(ctx, chatID)
	if err != nil {
		return err
	}
	sourceCh := make(chan models.Source, cfg.Feed.WCount)
	finished := make(chan bool)
	go func([]models.Source) {
		for _, s := range slS {
			sourceCh <- s
		}
		close(sourceCh)
	}(slS)
	go runWorkersPool(ctx, finished, cfg, sourceCh, repo)
	log.Println("Update chatID: ", chatID, "finished ", <-finished)
	return nil
}

func runWorkersPool(ctx context.Context, finished chan bool, cfg *config.Config, sCh chan models.Source, repo repo.Repository) {
	var w sync.WaitGroup
	for i := 0; i < cfg.Feed.WCount; i++ {
		w.Add(1)
		go worker(ctx, &w, sCh, repo, cfg)
	}
	w.Wait()
	finished <- true
}

func worker(ctx context.Context, w *sync.WaitGroup, sCh chan models.Source, repo repo.Repository, cfg *config.Config) {
	for s := range sCh {
		updateNews(ctx, s, repo, cfg)
	}
	w.Done()
}

func SkipNewsByKeyword(src []string, key string) []string {
	var dst = make([]string, 0, len(src))
	for _, s := range src {
		if strings.Contains(s, key) {
			dst = append(dst, s)
		}
	}
	return dst
}
