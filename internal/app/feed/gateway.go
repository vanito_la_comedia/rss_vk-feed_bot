package feed

import (
	"context"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log"
)

func NewGatewayServer(ctx context.Context, feedURL string) *runtime.ServeMux {

	mux := runtime.NewServeMux()
	opts := []grpc.DialOption{grpc.WithTransportCredentials(insecure.NewCredentials())}
	err := pb.RegisterFeedHandlerFromEndpoint(ctx, mux, feedURL, opts)
	if err != nil {
		log.Fatal("can't register gateway server")
	}

	return mux
}
