package feed

import (
	"context"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (f *feed) ListSource(ctx context.Context, req *pb.ListReq) (*pb.ListResponse, error) {

	slS, err := f.feedRepo.ListSource(ctx, req.ChatID)

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}
	var pbSl []*pb.Source
	for _, s := range slS {
		pbSl = append(pbSl, &pb.Source{
			ID:      s.ID,
			Type:    s.Type,
			URL:     s.URL,
			Keyword: s.Keyword,
		})
	}
	return &pb.ListResponse{
		Sources: pbSl,
	}, nil
}
