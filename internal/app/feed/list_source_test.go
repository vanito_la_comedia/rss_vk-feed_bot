package feed

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/gojuno/minimock/v3"
	"github.com/stretchr/testify/assert"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/mock"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
	"testing"
	"time"
)

func TestFeed_ListSource(t *testing.T) {
	mc := minimock.NewController(t)
	defer mc.Finish()

	mockRepo := mock.NewRepositoryMock(mc)
	sls := []models.Source{{1, "vk", "link", "key", sql.NullTime{Time: time.Now()}, 10}}
	fmt.Println(sls[0])
	mockRepo.ListSourceMock.Return(sls, nil)
	svc := New(mockRepo)

	ctx := context.Background()
	soReq := api.ListReq{ChatID: 10}
	resp, err := svc.ListSource(ctx, &soReq)

	assert.NoError(t, err)
	assert.Equal(t, sls[0].ID, resp.Sources[0].ID)
	assert.Equal(t, sls[0].URL, resp.Sources[0].URL)
	assert.Equal(t, sls[0].Keyword, resp.Sources[0].Keyword)
	assert.Equal(t, sls[0].Type, resp.Sources[0].Type)

}
