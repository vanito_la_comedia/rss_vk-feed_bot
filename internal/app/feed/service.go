package feed

import (
	repo "gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/repository"
	pb "gitlab.ozon.dev/vanito_la_comedia/homework-2/pkg/api"
)

type feed struct {
	feedRepo repo.Repository
	pb.UnimplementedFeedServer
}

func New(feedRepo repo.Repository) *feed {
	return &feed{feedRepo: feedRepo}
}
