package rss

import (
	"context"
	"database/sql"
	"fmt"
	"log"
	"time"

	"github.com/mmcdole/gofeed"
)

func GetNews(ctx context.Context, url string, updated sql.NullTime) ([]string, error) {
	var news []string

	ctx2, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()

	fp := gofeed.NewParser()
	feed, err := fp.ParseURLWithContext(url, ctx2)
	if err != nil {
		return nil, err
	}

	for _, item := range feed.Items {
		//log.Println("publ_date:=", item.PublishedParsed.Format(time.RFC822),
		//	"upd_time:=", updated.Time.Format(time.RFC822))
		if item.PublishedParsed.After(updated.Time) {
			log.Println("publ_date:=", item.PublishedParsed.Format(time.RFC822),
				"upd_time:=", updated.Time.Format(time.RFC822))
			it := fmt.Sprintf("%s \n\n %s \n\n %s \n\n %s \n\n Ссылка на оригинал: \n %s \n",
				feed.Title, item.PublishedParsed.Format(time.RFC822), item.Title, item.Description, item.Link)
			news = append(news, it)

		}
	}
	return news, nil
}
