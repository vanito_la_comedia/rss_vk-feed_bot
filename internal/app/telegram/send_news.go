package telegram

import (
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"log"
)

func SendOneNews(chatID int64, news string, token string) error {

	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		return err
	}

	bot.Debug = true

	log.Printf("Authorized on account %s", bot.Self.UserName)

	msg := tgbotapi.NewMessage(chatID, news)

	if _, err := bot.Send(msg); err != nil {
		log.Panic(err)
	}
	return nil
}

func SendSevNews(chatID int64, news []string, token string) error {
	for _, n := range news {
		err := SendOneNews(chatID, n, token)
		if err != nil {
			return err
		}
	}
	return nil
}
