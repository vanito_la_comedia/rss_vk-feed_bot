package vk

import (
	"context"
	"database/sql"
	"fmt"
	"github.com/SevereCloud/vksdk/v2/api"
	"github.com/ucarion/urlpath"
	"time"
)

func getScreenName(url string) (string, error) {
	path := urlpath.New("https://vk.com/:group")
	match, ok := path.Match(url)
	if !ok {
		return "", fmt.Errorf("error match group URL")
	}

	return match.Params["group"], nil
}

func getOwnerID(vk *api.VK, scrName string) (int, error) {
	resp, err := vk.UtilsResolveScreenName(api.Params{"screen_name": scrName})
	if err != nil {
		fmt.Println("can't resolve owner_id")
		return 0, err
	}
	return resp.ObjectID, nil
}

func getGroupName(vk *api.VK, ownerID int) (string, error) {
	grInfo, err := vk.GroupsGetByID(api.Params{"group_id": ownerID})
	if err != nil {
		fmt.Println("can't get group info")
		return "", err
	}
	return grInfo[0].Name, nil
}

func GetNews(ctx context.Context, url string, updated sql.NullTime, vkToken string) ([]string, error) {
	_, cancel := context.WithTimeout(ctx, 60*time.Second)
	defer cancel()
	var news []string

	scrName, err := getScreenName(url)
	if err != nil {
		return nil, err
	}

	vk := api.NewVK(vkToken)

	oID, err := getOwnerID(vk, scrName)
	if err != nil {
		return nil, err
	}

	grName, err := getGroupName(vk, oID)
	if err != nil {
		return nil, err
	}

	resp, err := vk.WallGet(api.Params{"owner_id": oID * (-1), "count": 10})
	if err != nil {
		fmt.Println("can't get wall news")
		return nil, err
	}
	for _, it := range resp.Items {

		/*		fmt.Println("publ_date:=", time.Unix(int64(it.Date), 0).Format(time.RFC822),
				"upd_time:=", updated.Time.Format(time.RFC822), it.ID)*/
		if time.Unix(int64(it.Date), 0).After(updated.Time) {
			itLink := fmt.Sprintf("https://vk.com/wall-%d_%d", oID, it.ID)
			item := fmt.Sprintf("%s \n\n %s \n\n %s \n\n Ссылка на оригинал: \n %s \n",
				grName, time.Unix(int64(it.Date), 0).Format(time.RFC822), it.Text, itLink)
			news = append(news, item)
		}
	}
	return news, nil
}
