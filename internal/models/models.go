package models

import "database/sql"

type Source struct {
	ID      int64
	Type    string
	URL     string
	Keyword string
	Updated sql.NullTime
	ChatID  int64
}
