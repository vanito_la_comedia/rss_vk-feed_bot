package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

func (r *repository) AddKeyword(ctx context.Context, source models.Source) (int64, error) {

	const query = `
		update sources
		set keyword=$1 
		where id = $2 and 
		chatID = $3
		returning id
	`
	var ID int64
	err := r.pool.QueryRow(ctx, query,
		source.Keyword,
		source.ID,
		source.ChatID,
	).Scan(&ID)
	if errors.Is(err, pgx.ErrNoRows) {
		ID = 0
		err = ErrNotFound
		return ID, err
	}
	if err != nil {
		return ID, fmt.Errorf("database: failed add keyword %v", err)
	}

	return ID, nil
}
