package repository

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

func (r *repository) AddSource(ctx context.Context, source models.Source) (int64, error) {

	const query = `
		insert into sources (
			type,
			URL,
			keyword,
			updated,
			chatID
		) VALUES (
			$1, $2, $3, now(), $4
		) returning id
	`
	var ID int64
	err := r.pool.QueryRow(ctx, query,
		source.Type,
		source.URL,
		"",
		source.ChatID,
	).Scan(&ID)

	if err != nil {
		return ID, fmt.Errorf("database: failed add source %v", err)
	}

	return ID, nil
}
