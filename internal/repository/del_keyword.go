package repository

import (
	"context"
	"errors"
	"fmt"
	"github.com/jackc/pgx/v4"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

func (r *repository) DelKeyword(ctx context.Context, source models.Source) error {

	const query = `
		update sources
		set keyword=$1 
		where id = $2 and 
		chatID = $3
	`

	cmd, err := r.pool.Exec(ctx, query,
		"",
		source.ID,
		source.ChatID,
	)
	if errors.Is(err, pgx.ErrNoRows) {
		err = ErrNotFound
		return err
	}
	if err != nil {
		return fmt.Errorf("database: failed del keyword %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}
