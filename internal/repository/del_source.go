package repository

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

func (r *repository) DelSource(ctx context.Context, source models.Source) error {

	const query = `
		delete from sources
		where id = $1 and chatID=$2;
	`

	cmd, err := r.pool.Exec(ctx, query, source.ID, source.ChatID)
	if err != nil {
		return fmt.Errorf("database: failed del source %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}
	return nil
}
