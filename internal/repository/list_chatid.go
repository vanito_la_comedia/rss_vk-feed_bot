package repository

import (
	"context"
)

func (r *repository) ListChatID(ctx context.Context) ([]int64, error) {

	const query = `
		select distinct chatID
		from sources;
	`

	rows, err := r.pool.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var chatIDs []int64
	for rows.Next() {
		var u int64
		if err = rows.Scan(
			&u,
		); err != nil {
			return nil, err
		}

		chatIDs = append(chatIDs, u)
	}

	return chatIDs, nil
}
