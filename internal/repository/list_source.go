package repository

import (
	"context"
	"fmt"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

func (r *repository) ListSource(ctx context.Context, ID int64) (slSource []models.Source, err error) {

	const query = `
		select id,
			type,
			URL,
			keyword,
			updated,
			chatID
		from sources
		where chatID = $1;
	`

	rows, err := r.pool.Query(ctx, query, ID)
	if err != nil {
		return nil, fmt.Errorf("database: failed list source %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var source models.Source
		if err = rows.Scan(
			&source.ID,
			&source.Type,
			&source.URL,
			&source.Keyword,
			&source.Updated,
			&source.ChatID,
		); err != nil {
			return nil, fmt.Errorf("can't read source from database responce %v", err)
		}

		slSource = append(slSource, source)
	}

	return slSource, nil
}
