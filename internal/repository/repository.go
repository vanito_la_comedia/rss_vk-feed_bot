package repository

import (
	"context"
	"errors"
	"github.com/jackc/pgx/v4/pgxpool"
	"gitlab.ozon.dev/vanito_la_comedia/homework-2/internal/models"
)

var ErrNotFound = errors.New("not found")

type Repository interface {
	AddSource(context.Context, models.Source) (int64, error)
	DelSource(context.Context, models.Source) error
	AddKeyword(context.Context, models.Source) (int64, error)
	DelKeyword(context.Context, models.Source) error
	ListSource(context.Context, int64) ([]models.Source, error)
	ListChatID(context.Context) ([]int64, error)
	UpdateTime(context.Context, int64) error
}

type repository struct {
	pool *pgxpool.Pool
}

func New(pool *pgxpool.Pool) *repository {
	return &repository{pool: pool}
}
