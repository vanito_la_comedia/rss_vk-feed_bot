package repository

import (
	"context"
	"fmt"
)

func (r *repository) UpdateTime(ctx context.Context, id int64) error {

	const query = `
		update sources
		set updated=now() 
		where id = $1
	`

	cmd, err := r.pool.Exec(ctx, query, id)
	if err != nil {
		return fmt.Errorf("database: failed update time %v", err)
	}
	if cmd.RowsAffected() == 0 {
		err = ErrNotFound
		return err
	}

	return nil
}
