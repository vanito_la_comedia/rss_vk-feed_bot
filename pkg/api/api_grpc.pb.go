// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.15.8
// source: api/api.proto

package api

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
	emptypb "google.golang.org/protobuf/types/known/emptypb"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// FeedClient is the client API for Feed service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type FeedClient interface {
	ListSource(ctx context.Context, in *ListReq, opts ...grpc.CallOption) (*ListResponse, error)
	AddSource(ctx context.Context, in *AddSoRequest, opts ...grpc.CallOption) (*ID, error)
	DelSource(ctx context.Context, in *ID, opts ...grpc.CallOption) (*emptypb.Empty, error)
	AddKeyword(ctx context.Context, in *AddKeyRequest, opts ...grpc.CallOption) (*ID, error)
	DelKeyword(ctx context.Context, in *ID, opts ...grpc.CallOption) (*emptypb.Empty, error)
}

type feedClient struct {
	cc grpc.ClientConnInterface
}

func NewFeedClient(cc grpc.ClientConnInterface) FeedClient {
	return &feedClient{cc}
}

func (c *feedClient) ListSource(ctx context.Context, in *ListReq, opts ...grpc.CallOption) (*ListResponse, error) {
	out := new(ListResponse)
	err := c.cc.Invoke(ctx, "/api.Feed/ListSource", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedClient) AddSource(ctx context.Context, in *AddSoRequest, opts ...grpc.CallOption) (*ID, error) {
	out := new(ID)
	err := c.cc.Invoke(ctx, "/api.Feed/AddSource", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedClient) DelSource(ctx context.Context, in *ID, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.Feed/DelSource", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedClient) AddKeyword(ctx context.Context, in *AddKeyRequest, opts ...grpc.CallOption) (*ID, error) {
	out := new(ID)
	err := c.cc.Invoke(ctx, "/api.Feed/AddKeyword", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *feedClient) DelKeyword(ctx context.Context, in *ID, opts ...grpc.CallOption) (*emptypb.Empty, error) {
	out := new(emptypb.Empty)
	err := c.cc.Invoke(ctx, "/api.Feed/DelKeyword", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// FeedServer is the server API for Feed service.
// All implementations must embed UnimplementedFeedServer
// for forward compatibility
type FeedServer interface {
	ListSource(context.Context, *ListReq) (*ListResponse, error)
	AddSource(context.Context, *AddSoRequest) (*ID, error)
	DelSource(context.Context, *ID) (*emptypb.Empty, error)
	AddKeyword(context.Context, *AddKeyRequest) (*ID, error)
	DelKeyword(context.Context, *ID) (*emptypb.Empty, error)
	mustEmbedUnimplementedFeedServer()
}

// UnimplementedFeedServer must be embedded to have forward compatible implementations.
type UnimplementedFeedServer struct {
}

func (UnimplementedFeedServer) ListSource(context.Context, *ListReq) (*ListResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListSource not implemented")
}
func (UnimplementedFeedServer) AddSource(context.Context, *AddSoRequest) (*ID, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddSource not implemented")
}
func (UnimplementedFeedServer) DelSource(context.Context, *ID) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DelSource not implemented")
}
func (UnimplementedFeedServer) AddKeyword(context.Context, *AddKeyRequest) (*ID, error) {
	return nil, status.Errorf(codes.Unimplemented, "method AddKeyword not implemented")
}
func (UnimplementedFeedServer) DelKeyword(context.Context, *ID) (*emptypb.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DelKeyword not implemented")
}
func (UnimplementedFeedServer) mustEmbedUnimplementedFeedServer() {}

// UnsafeFeedServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to FeedServer will
// result in compilation errors.
type UnsafeFeedServer interface {
	mustEmbedUnimplementedFeedServer()
}

func RegisterFeedServer(s grpc.ServiceRegistrar, srv FeedServer) {
	s.RegisterService(&Feed_ServiceDesc, srv)
}

func _Feed_ListSource_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedServer).ListSource(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Feed/ListSource",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedServer).ListSource(ctx, req.(*ListReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _Feed_AddSource_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddSoRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedServer).AddSource(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Feed/AddSource",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedServer).AddSource(ctx, req.(*AddSoRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Feed_DelSource_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedServer).DelSource(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Feed/DelSource",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedServer).DelSource(ctx, req.(*ID))
	}
	return interceptor(ctx, in, info, handler)
}

func _Feed_AddKeyword_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(AddKeyRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedServer).AddKeyword(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Feed/AddKeyword",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedServer).AddKeyword(ctx, req.(*AddKeyRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Feed_DelKeyword_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ID)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(FeedServer).DelKeyword(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/api.Feed/DelKeyword",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(FeedServer).DelKeyword(ctx, req.(*ID))
	}
	return interceptor(ctx, in, info, handler)
}

// Feed_ServiceDesc is the grpc.ServiceDesc for Feed service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var Feed_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "api.Feed",
	HandlerType: (*FeedServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ListSource",
			Handler:    _Feed_ListSource_Handler,
		},
		{
			MethodName: "AddSource",
			Handler:    _Feed_AddSource_Handler,
		},
		{
			MethodName: "DelSource",
			Handler:    _Feed_DelSource_Handler,
		},
		{
			MethodName: "AddKeyword",
			Handler:    _Feed_AddKeyword_Handler,
		},
		{
			MethodName: "DelKeyword",
			Handler:    _Feed_DelKeyword_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "api/api.proto",
}
